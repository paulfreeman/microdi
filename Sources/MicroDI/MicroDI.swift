//
//  MicroDI.swift
//
//  Created by Paul Freeman on 09/11/2020.
//
//  Free to use, no licensing requirements
//
//  Rocket Garden Labs, 2020
//
//  Use at your own risk as a starting point to develop a DI container
//  provides minimal service registration and resolution by type
//
    
struct RegistryEntry {
    let entry: Any
    let name : String?
    let implementing : String?
}

struct Registry{
    var registrations =  Dictionary<Int, RegistryEntry>()
}
 
/// Super minimal DIContainer providing a service registry
///
/// Basic usage is to :
/// Initialise a MicroDI instance and store a reference
/// register services with the container
/// resolve (locate) services and use them in client code
///
public class MicroDI {
     
    internal var registry = Registry()
    
    ///Expose to to package consumers
    public init() {}
     
    ///Register an instance of a service
    ///
    /// ```
    /// microDI.register(UserService())
    /// ```
    /// Parameter service : an instance of Service
    /// Parameter name    : an optional name to use for resolving the service
    ///
    public func register<Service>(_ item: Service, name: String? = nil){
        let key = ObjectIdentifier(Service.self).hashValue
        registry.registrations[key] = RegistryEntry(entry:item, name:name, implementing:nil)
    }
    
    ///Register an instance of a service that implements a given protocol
    ///
    /// ```
    /// microDI.register(UserService(), implementing:UserServiceProtocol.self, name:"userService")
    /// ```
    /// Parameter service : an instance of Service
    /// Parameter implementing : A protocol type
    ///
    public func register<Service, Protocol>(_ item: Service, implementing: Protocol.Type, name: String? = nil){
        let key = ObjectIdentifier(Service.self).hashValue
        registry.registrations[key] = RegistryEntry(entry:item, name:name, implementing:"\(implementing)")
    }
     
    /// Find an instance of a service that was previously registered as an instance
    ///
    /// ```
    /// microDI.resolve(UserService.self)
    /// ```
    /// Parameter service : the type of the service to locate
    /// Returns: A Swift Result containing either a Service or a MicroDIError
    ///
    public func resolve<Service>(_ service:Service.Type) ->  Service? {
        let key = ObjectIdentifier(service).hashValue
        guard let resolved = registry.registrations[key]  else {  return nil }
        return resolved.entry as? Service
    }
    
    /// Find an instance of a service that was previously registered with a name
    ///
    /// ```
    /// microDI.resolve(named:"userService")
    /// ```
    /// Parameter service : the type of the service to locate
    /// Returns: An instance of the inferred Service type
    ///
    public func resolve<Service>(named name:String) ->  Service? {
        let resolutions = registry.registrations.values.filter({$0.name == name })
        let item = resolutions.first
        return item?.entry  as? Service
    }
    
    /// Find an instance of a service that was previously registered with a protocol type
    ///
    /// ```
    /// microDI.resolve(protocolName:"UserServiceProtocol") as? UserServiceProtocol
    /// ```
    /// This needs a cast to the desired protocol type as the return value is Any?
    ///
    /// Parameter service : the type of the service to locate
    /// Returns: An type erased instance of a type conforming to the protocol name provided as a string
    ///
    /// It is possible to register multiple types that conform to a protocol. This method simply
    /// returns the first one found. If this is not the required behaviour, then resolving with
    /// resolve(named: "testUserService") may be the better alternative.
    ///
    public func resolve(protocolName:String) -> Any? {
        let resolutions = registry.registrations.values.filter({$0.implementing == "\(protocolName)"})
        let item = resolutions.first
        return item?.entry
    }
    
    
}
