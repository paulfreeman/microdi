# MicroDI
  
A minimal implementation of an IoC container for registering and resolving services 

- Services have to be registered prior to use
- No lazily initialised services 
- Services are registered only in global app scope ()
- MicroDI registry must be retained at the global application scope
- No transitive dependencies
- No support for transient, session or other scopes
   
Services are registered by Object identifier as a key.  This means that only a single instance of a class or struct can be registered at a time. 
If an attempt is made to register the same object twice,  the second registration will overwrite the first. 

## Usage   
 
## Setup 

Initialise a MicroDI instance and store a reference to it in the client 

eg : in the AppDelegate 

```
var microDI = MicroDI() 
``` 
 ## Register services by type
  
Register services with the container for future use. The simplest case just registers an object, one instance per class or struct, resolution is via the object type

```
let myService = MyService()
microDI.register(service : myService)
```

## Find and use services registered by type

resolve (locate) services and use them in client code
 
```
let locatedService = microDI.resolve(service: MyService.self) 
locatedService.serviceMethod()

``` 

## Register with a protocol and a name

A more useful approach is to register services by protocol and a name for retrieval. 
This allows resolution to the specified protocol
Note that two different object id's can be registered for the same Protocol. 
In this case, use of a distinct name for each Protocol implementation can be used to resolve the desired object. 
For example a prototyped or mock version as well as a production version. 

```
let myService = MyService()
let myTestService = MyTestService() 
di.register(myService,     implementing: MyProtocol.self, name:"mySearchService")
di.register(myTestService, implementing: MyProtocol.self, name:"myMockSearchService")
```

## Resolve protocol by name  
 
```
let locatedService: MyProtocol? = di.resolve(named:"mySearchService") 
locatedService?.protocolMethod()
```


