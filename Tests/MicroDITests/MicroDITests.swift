//
//  MicroDITests.swift
//
//  Created by Paul Freeman on 09/11/2020.
//

import XCTest
@testable import MicroDI 

protocol HTTPServiceProtocol   {
    func get(request:Any) -> String
}

struct SearchServiceStub : HTTPServiceProtocol  {
    func get(request:Any) ->  String {
        return "Stub Result"
    }
}

class SearchService: HTTPServiceProtocol {
    func get(request:Any) -> String {
        return  "Actual Result"
    }
}
 
final class MicroDITests: XCTestCase {
    
    static var di:MicroDI = MicroDI()
    
     
    ///registering by type simply add a type
    func test_register_by_type() {
        //assuming
        let di = MicroDI()
        let service = SearchService()
        di.register(service)
        
        //when
        let myService:SearchService? = di.resolve(SearchService.self)
        
        //then
        XCTAssertNotNil(myService)
        XCTAssertEqual(myService?.get(request: "foo"), "Actual Result")
        
        //assuming
        let stub = SearchServiceStub()
        
        //when
        di.register(stub)
        let myStub:SearchServiceStub? = di.resolve(SearchServiceStub.self)
        
        //then
        XCTAssertNotNil(myStub)
        XCTAssertEqual(myStub?.get(request: "foo"), "Stub Result")
    }
    
    func test_register_with_name_resolve_with_type() {
        //assuming
        let di = MicroDI()
        let service = SearchService()
        di.register(service, name: "SearchService1")
        
        //when
        let myService:SearchService? = di.resolve(SearchService.self)
        
        //then
        XCTAssertNotNil(myService)
        XCTAssertEqual(myService?.get(request: "foo"), "Actual Result")
    }
    
    func test_resolve_with_name() {
        //assuming
        let di = MicroDI()
        let service = SearchService()
        di.register(service, name: "SearchService1")

        //when
        let myService:SearchService? = di.resolve(named:"SearchService1")

        //then
        XCTAssertNotNil(myService)
        XCTAssertEqual(myService?.get(request: "foo"), "Actual Result")
    }
     
    func test_resolve_missing() {
        //assuming
        let di = MicroDI()
        let service = SearchService()
        di.register(service)
        
        //when
        let myStub:SearchServiceStub? = di.resolve(SearchServiceStub.self)
        
        //then
        XCTAssertNil(myStub)
        XCTAssertNotEqual(myStub?.get(request: "foo"), "Stub Result")
    }
 
    func test_register_type_of_protocol() {
        //assuming
         let di = MicroDI()
         let service = SearchService()
 
         di.register(service, implementing: HTTPServiceProtocol.self)

         //when
         let myService:HTTPServiceProtocol? = di.resolve(protocolName:"HTTPServiceProtocol") as? HTTPServiceProtocol

         //then
         XCTAssertNotNil(myService)
         XCTAssertEqual(myService?.get(request: "foo"), "Actual Result")
    }
    
    func test_register_named_services_with_same_protocol_single_instance_registered() {
        //assuming
        let di = MicroDI()
        let service1 = SearchService()
        let service2 = SearchService()
        
        di.register(service1, implementing: HTTPServiceProtocol.self, name:"elasticSearchService")
        di.register(service2, implementing: HTTPServiceProtocol.self, name:"databaseSearchService")
         
        XCTAssertEqual(di.registry.registrations.count, 1)
        
        //when
        let myService1:HTTPServiceProtocol? = di.resolve(named:"elasticSearchService")
        let myService2:HTTPServiceProtocol? = di.resolve(named:"databaseSearchService")
        
        //then
        XCTAssertNil(myService1)
        XCTAssertNotNil(myService2)
    }
     
    
    func test_register_named_services_with_same_protocol_different_instance_types_registered() {
        //assuminge
        let di = MicroDI()
        let service1 = SearchService()
        let service2 = SearchServiceStub()
        
        di.register(service1, implementing: HTTPServiceProtocol.self, name:"elasticSearchService")
        di.register(service2, implementing: HTTPServiceProtocol.self, name:"testSearchService")
        
        XCTAssertEqual(di.registry.registrations.count, 2)
        
        //when
        let myService1:HTTPServiceProtocol? = di.resolve(named:"elasticSearchService")
        let myService2:HTTPServiceProtocol? = di.resolve(named:"testSearchService")
        
        //then
        XCTAssertNotNil(myService1)
        XCTAssertNotNil(myService2)
    }
    
    
    static var allTests = [
        ("test_register_by_type", test_register_by_type),
        ("test_register_with_name_resolve_with_type", test_register_with_name_resolve_with_type),
        ("test_resolve_missing",test_resolve_missing),
        ("test_resolve_with_name", test_resolve_with_name),
        ("test_register_type_of_protocol", test_register_type_of_protocol),
        ("test_register_named_services_with_same_protocol_single_instance_registered", test_register_named_services_with_same_protocol_single_instance_registered),
        ("test_register_named_services_with_same_protocol_different_instance_types_registered", test_register_named_services_with_same_protocol_different_instance_types_registered)
        
    ]
}
