import XCTest

import MicroDITests

var tests = [XCTestCaseEntry]()
tests += MicroDITests.allTests()
XCTMain(tests)
